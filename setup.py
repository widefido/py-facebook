#!/usr/bin/env python

from distutils.core import setup

setup(name="facebook",
      description="simple facebook graph api wrapper",
      url="",
      version="1.0",
      author="Facebook",
      author_email="",
      py_modules=["facebook"]
     )
